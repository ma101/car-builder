package cpit252;

public interface CarBuilder {

    public CarBuilder BuildEngine();

    public CarBuilder BuildBody();

    public CarBuilder BuildTires();

    public CarBuilder BuildBattery();

    public Car build();

}
